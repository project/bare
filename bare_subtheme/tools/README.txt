# Automation Tools

Here at Bare Headquarters, we don't give a rats fart what you use to compile,
just as long as it is automated, helps keep things clean and sanitary, and makes
your life a freaking piece of cake. For that reason, we have include two folders
that might ease your set up of the main stream automation tools but are not
required to run bare.

## Grunt

To get started with grunt:

 1. copy all the files in tools/grunt to the base of your theme.
 2. run npm install; npm update;

## Gulp
Coming in the future.
